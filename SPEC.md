# Pulse Networking Specification

This document provides a few things:

1. An overview of the protocol.
2. The nitty-gritty details.

## Overview

The Pulse Networking Protocol is built on top of TCP/IP and UDP/IP due to their exceptional support, documentation, and ease of use. 

### Packets

> Note: hexidecimal such as `0x10` doesn't refer to the string literal "0x10", but rather a byte with value `0b00010000` which is 16 in decimal. Keep in mind that two hexidecimal characters represent one byte.

Data is split into packets. These packets start and end with a null byte (`0x00`). The packet identifier uses two bytes. This provides enough unique packet ids, because I can't imagine a project that would need more than *65,535* packet ids (if you do need more, please create an issue on GitLab and include a description on what you did to need that many, seriously).

#### Reserved Packet IDs

The following packet IDs shouldn't be used for anything else.

|       Usage       |   Hex  |      Bin     |
|:-----------------:|:------:|:------------:|
|      Connect      | `0x01` | `0b00000001` |
|     Disconnect    | `0x02` | `0b00000010` |
|      Hearbeat     | `0x03` | `0b00000011` |
| Request Heartbeat | `0x04` | `0b00000100` |

#### Data Types

Each packet can send one of the following data types. Most of the data types below (all except "Raw Data") are C++ types, either builtin or available from the official library. 

> Booleans should be represented as raw data

|              Type             |   Hex  |      Bin     |
|:-----------------------------:|:------:|:------------:|
|            Raw Data           | `0x01` | `0b00000001` |
|              int              | `0x02` | `0b00000010` |
|             double            | `0x03` | `0b00000011` |
|             float             | `0x04` | `0b00000100` |
|              long             | `0x05` | `0b00000101` |
|           long long           | `0x06` | `0b00000110` |
|          unsigned int         | `0x07` | `0b00000111` |
|         unsigned long         | `0x08` | `0b00001000` |
|       unsigned long long      | `0x09` | `0b00001001` |
|              char             | `0x0a` | `0b00001010` |
|         unsigned char         | `0x0b` | `0b00001011` |
|             int8_t            | `0x0c` | `0b00001100` |
|            int16_t            | `0x0d` | `0b00001101` |
|            int32_t            | `0x0e` | `0b00001110` |
|            int64_t            | `0x0f` | `0b00001111` |
|            uint8_t            | `0x10` | `0b00010000` |
|            uint16_t           | `0x11` | `0b00010001` |
|            uint32_t           | `0x12` | `0b00010010` |
|            uint64_t           | `0x13` | `0b00010011` |
|             char*             | `0x14` | `0b00010100` |
| pulse_networking::types::vec2 | `0x15` | `0b00010101` |
| pulse_networking::types::vec3 | `0x16` | `0b00010110` |
| pulse_networking::types::vec4 | `0x17` | `0b00010111` |
| pulse_networking::types::utf8 | `0x18` | `0b00011000` |

#### Constructing a Packet

The packet format is relatively simple. The first byte should be `0x00` to let the server/client know a packet is starting. After that is the packet identifier, which is two bytes. Next is the data type, which is one byte. Refer to the <a href="#data-types">data type table</a> for what value should be used for what data type. Finally, simply append the data. 
