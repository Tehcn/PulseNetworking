#include <iostream>
#include "tcp.h"

using namespace pulse_networking;

const int PORT = 42069;

int main() {
	tcp::Server server = tcp::create_server(PORT);
	int r = tcp::bind_server(server);

	if(r != PORT) {
		std::cerr << "Failed to bind on port " << PORT << '\n';
		return 1;
	}

	tcp::listen(server);

	return 0;
}
