#include "tcp.h"

#include <iostream>
#include <string>


pulse_networking::tcp::Server pulse_networking::tcp::create_server(int port) {
	zsock_t* socket = zsock_new(ZMQ_REP);
	return Server{port, socket};
}

int pulse_networking::tcp::bind_server(pulse_networking::tcp::Server server) {
	return zsock_bind(server.socket, ("tcp://localhost" + std::to_string(server.port)).c_str());
}

void pulse_networking::tcp::listen(pulse_networking::tcp::Server server) {
	while(true) {
		zframe_t* frame = zframe_recv(socket);
		if(!frame) {
			pulse_networking::tcp::close_with_error(server, 2);
		}

		byte* data = zframe_data(frame);
		size_t size = zframe_size(frame);

		

		zframe_destroy(&frame);
	}
}

void pulse_networking::tcp::add_listener(pulse_networking::tcp::Server server, pulse_networking::tcp::ListenerFunc func, int packet_id) {
	server.listeners.push_back(pulse_networking::tcp::Listener{func, packet_id});
}

[[noreturn]] void pulse_networking::tcp::close_with_error(pulse_networking::tcp::Server server, int code) {
	zsock_destroy(&server.socket);
	exit(code);
}