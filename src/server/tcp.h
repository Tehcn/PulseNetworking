#ifndef PULSE_TCP_H
#define PULSE_TCP_H

#include <vector>
#include <czmq.h>
#include <vector>

namespace pulse_networking::tcp {
	enum DataType {
		Raw,
		Int,
		Double,
		Float,
		Long,
		LongLong,
		UnsignedInt,
		UnsignedLong,
		UnsignedLongLong,
		Char,
		UnsignedChar,
		Int8_t,
		Int16_t,
		Int32_t,
		Int64_t,
		Uint8_t,
		Uint16_t,
		Uint32_t,
		Uint64_t,
		// pulse specific types
		P_Vec2,
		P_Vec3,
		P_Vec4,
		P_UTF8
	};

	typedef void (*ListenerFunc)(byte*,size_t);

	typedef struct  {
		ListenerFunc func;
		int packet_id;
	} Listener;

	typedef struct {
		int id;
	} Connection;

	typedef struct {
		int port;
		zsock_t* socket;
		std::vector<Listener> listeners;
	} Server;

	Server create_server(int port);
	int bind_server(Server server);
	void listen(Server server);
    void add_listener(pulse_networking::tcp::Server server, pulse_networking::tcp::ListenerFunc func, int packet_id);
    void close_with_error(Server server, int code);

	void _get_packet_id(byte* data, size_t size);
	void _get_packet_data_type(byte* data, size_t size);
	void _get_packet_data_with_type(byte* data, size_t size, int type);
}

#endif
